function newToDo() {
    //RECUPERATION DE L'EXISTANT DANS INDEX.HTML
    var list = document.getElementById("list");
    var input = document.getElementById("input");

    //CREATION DES ELEMENTS NECESSAIRE A LA LISTE 
    var li = document.createElement("li");
    var span = document.createElement("span");
    var inputValue = input.value;
    var text = document.createTextNode(inputValue);
    var buttonDel = document.createElement("button");

    //CONFIGURATION DU BOUTTON SUPPRIMER
    buttonDel.type = "submit";
    buttonDel.innerHTML = "Supprimer"

   /*  TEST SI LE CHAMP DU FORMULAIRE EST COMPLETE
    ET INSERTION DANS LA PAGE WEB */
    if (inputValue === "") {
        alert("Remplissez le champ texte");
    } else {
        list.appendChild(li);
        li.appendChild(buttonDel);
        li.appendChild(span);        
        span.appendChild(text);
        input.value="";
    }

    //BOUTON SUPPRIMER
    buttonDel.onclick = function(){
        li.parentNode.removeChild(li);
    };

    //BARRE LE TEXTE EN CLIQUANT DESSUS
    span.onclick = function(){
        this.classList.toggle("barre");
    }; 
}

